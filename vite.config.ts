import postcssPresetEnv from "postcss-preset-env";
import path from "path";
import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";

import { peerDependencies } from "./package.json";

export default defineConfig({
  plugins: [solidPlugin()],
  css: {
    postcss: {
      plugins: [
        postcssPresetEnv({
          features: {
            "custom-media-queries": true,
            "nesting-rules": true,
          },
          importFrom: [
            {
              customMedia: {
                "--sm": "(min-width: 576px)",
                "--md": "(min-width: 768px)",
                "--lg": "(min-width: 992px)",
                "--xl": "(min-width: 1200px)",
                "--light-mode": "(prefers-color-scheme: light)",
                "--lg-light-mode":
                  "(min-width: 992px) and (prefers-color-scheme: light)",
                "--dark-mode": "(prefers-color-scheme: dark)",
              },
            },
          ],
        }),
      ],
    },
  },
  build: {
    lib: {
      entry: path.resolve(__dirname, "src/lib.tsx"),
      name: "TimadaUI",
    },
    rollupOptions: {
      external: Object.keys(peerDependencies),
      output: {
        globals: {
          flexsearch: "Flexsearch",
          immer: "Immer",
          "solid-js": "SolidJs",
          "solid-app-router": "SolidAppRouter",
        },
      },
    },
  },
  resolve: {
    alias: [
      { find: /^(lib|icons|utils)/, replacement: "/src/$1" },
      { find: /^(pages|components|icons)\//, replacement: "/src/$1/" },
    ],
  },
});
