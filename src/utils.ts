import { onCleanup } from "solid-js";

export function createDebounce<T extends unknown[]>(
  fn: (...args: T) => void,
  ms: number
) {
  let timeout: number | undefined;

  const cancel = () => {
    clearTimeout(timeout);
  };

  onCleanup(cancel);

  return (...args: T) => {
    cancel();

    timeout = setTimeout(() => {
      fn(...args);
    }, ms);
  };
}
