import { Component, JSX } from "solid-js";

const Input: Component<JSX.InputHTMLAttributes<HTMLInputElement>> = (props) => {
  return <input {...props} />;
};

export default Input;
