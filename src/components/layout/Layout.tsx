import {
  Component,
  createEffect,
  createSignal,
  For,
  JSX,
  Show,
  Accessor,
  Setter,
} from "solid-js";

import { Document } from "flexsearch";
import { NavLink } from "solid-app-router";
import produce from "immer";
import Drawer from "components/drawer/Drawer";
import Button from "components/button/Button";
import Input from "components/input/Input";
import { Abs015, Abs030, Com009 } from "icons";

import "./Layout.css";

export interface LayoutService {
  name: string;
  href: string;
  logo: string;
}

export interface LayoutNav {
  title?: JSX.Element;
  items: LayoutNavItem[];
}

export interface LayoutNavItem {
  label: JSX.Element;
  href: string;
  icon?: JSX.Element;
}

export interface LayoutProps {
  header: JSX.Element;
  navigations: LayoutNav[];
  services: LayoutService[];
}

const FAVORITE_KEY = "service_favorites";

function createLocalFavorites(
  services: LayoutService[]
): [Accessor<Map<string, LayoutService>>, Setter<Map<string, LayoutService>>] {
  const [favorites, setFavorites] = createSignal(new Map());

  const serviceFavorite = localStorage.getItem(FAVORITE_KEY);

  createEffect(() =>
    localStorage.setItem(FAVORITE_KEY, JSON.stringify([...favorites()]))
  );

  if (!serviceFavorite) {
    return [favorites, setFavorites];
  }

  const value = new Map(JSON.parse(serviceFavorite));

  services.forEach((service) => {
    if (value.has(service.name)) {
      value.set(service.name, service);
    }
  });

  setFavorites(value);

  return [favorites, setFavorites];
}

const Layout: Component<LayoutProps> = (props) => {
  const [sidebarOpen, setSidebarOpen] = createSignal(false);
  const [appbarOpen, setAppbarOpen] = createSignal(false);
  const [editFavorite, setEditFavorite] = createSignal(false);
  const [filteredServices, setFilteredServices] = createSignal(props.services);
  const [favorites, setFavorites] = createLocalFavorites(props.services);

  const serviceIndex = props.services.reduce(
    (acc, v) => acc.add(v),
    new Document<LayoutService, string[]>({
      document: { id: "name", index: "name", store: ["name", "href", "logo"] },
      tokenize: "forward",
    })
  );

  const onSearchInput: JSX.EventHandler<HTMLInputElement, InputEvent> = async (
    e
  ) => {
    const { value } = e.currentTarget;
    const filtered = await serviceIndex.searchAsync(value, {
      enrich: true,
    });

    const filteredServices = filtered[0]?.result.map(({ doc }) => doc);

    setFilteredServices(value ? filteredServices : props.services);
  };

  const onToggleFavoriteService = (service: LayoutService) => {
    setFavorites(
      produce(favorites(), (draft) => {
        if (favorites().has(service.name)) {
          draft.delete(service.name);
        } else {
          draft.set(service.name, service);
        }
      })
    );
  };

  const onAppbarClose = () => {
    setAppbarOpen(false);
    setEditFavorite(false);
  };

  return (
    <div class="layout">
      <Drawer
        classList={{ layout__drawer: true }}
        open={sidebarOpen()}
        onClose={() => setSidebarOpen(false)}
      >
        <div class="layout__sidebar">
          <div class="layout__userbar">
            <ul>
              <li>
                <Button
                  class="layout__service-button"
                  variant="transparent"
                  onClick={() => setAppbarOpen(true)}
                >
                  <Abs030 />
                </Button>
              </li>
              <For each={Array.from(favorites())}>
                {([, { name, href, logo }]) => (
                  <li class="layout__service-item">
                    <a href={href} title={name}>
                      <img src={logo} alt={name} />
                    </a>
                  </li>
                )}
              </For>
            </ul>
            <ul>
              {/* <img
                  class={styles.layout__sidebar__nav__item}
                  src="https://robohash.org/snapiz.png"
                  alt="timada log"
                /> */}
            </ul>
          </div>
          <div class="layout__appbar">
            <h1>{props.header}</h1>
            <For each={props.navigations}>
              {({ title, items }) => (
                <>
                  <Show when={title}>
                    <h3 class="layout__nav-title">{title}</h3>
                  </Show>
                  <ul class="layout__nav-items">
                    <For each={items}>
                      {({ label, href, icon }) => (
                        <li
                          class="layout__nav-item"
                          onclick={() => setSidebarOpen(false)}
                        >
                          <NavLink
                            href={href}
                            activeClass="layout__nav-link--active"
                            end
                          >
                            <Show when={icon}>
                              <span class="layout__nav-item__icon">{icon}</span>
                            </Show>
                            <span class="layout__nav-icon__label">{label}</span>
                          </NavLink>
                        </li>
                      )}
                    </For>
                  </ul>
                </>
              )}
            </For>
          </div>
        </div>
      </Drawer>
      <Drawer
        placement={["right", "right", "left"]}
        open={appbarOpen()}
        onClose={onAppbarClose}
      >
        <div class="layout__favorite-search">
          <Input
            type="search"
            oninput={onSearchInput}
            placeholder="type to search"
          />
          <Button
            variant="transparent"
            classList={{
              "layout__favorite-toggle": true,
              "layout__favorite-toggle--selected": editFavorite(),
            }}
            onClick={() => setEditFavorite((v) => !v)}
          >
            <Com009 />
          </Button>
        </div>
        <ul class="layout__favorite-menu">
          <For each={filteredServices()}>
            {(service) => (
              <li
                classList={{
                  "layout__favorite-menu-item": true,
                  "layout__favorite-menu-item--selected":
                    editFavorite() && favorites().has(service.name),
                }}
              >
                <Show
                  when={!editFavorite()}
                  fallback={
                    <Button
                      variant="transparent"
                      onClick={[onToggleFavoriteService, service]}
                    >
                      <img src={service.logo} alt={service.name} />
                      <div>{service.name}</div>
                    </Button>
                  }
                >
                  <a href={service.href} title={service.name}>
                    <img src={service.logo} alt={service.name} />
                    <div>{service.name}</div>
                  </a>
                </Show>
              </li>
            )}
          </For>
        </ul>
      </Drawer>
      <div class="layout__bottombar">
        <div>
          <div>
            <Button variant="transparent" onclick={() => setSidebarOpen(true)}>
              <Abs015 />
            </Button>
          </div>
          <div></div>
          <div>
            <Button variant="transparent" onclick={() => setAppbarOpen(true)}>
              <Abs030 />
            </Button>
          </div>
        </div>
      </div>
      <div class="layout__content">{props.children}</div>
    </div>
  );
};

export default Layout;
