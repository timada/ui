import { Component, createEffect, JSX, splitProps } from "solid-js";
import { Portal } from "solid-js/web";

import "./Drawer.css";

type Placement = "left" | "right" | "bottom" | "top";

export interface DrawerProps {
  placement?: Placement | Placement[];
  open: boolean;
  onClose: () => void;
  contentProps?: JSX.HTMLAttributes<HTMLDivElement>;
  overlayProps?: JSX.HTMLAttributes<HTMLDivElement>;
}

const Drawer: Component<DrawerProps & JSX.HTMLAttributes<HTMLDivElement>> = (
  props
) => {
  const [local, others] = splitProps(props, [
    "placement",
    "open",
    "onClose",
    "children",
    "classList",
    "contentProps",
    "overlayProps",
  ]);

  const [placement, mdPlacement, lgPlacement] =
    typeof local.placement === "string"
      ? [local.placement]
      : local.placement || ["left"];

  createEffect(() => {
    document.body.style.overflow = local.open ? "hidden" : "";
  });

  return (
    <Portal mount={document.getElementById("drawer") || undefined}>
      <div
        classList={{
          "drawer--open": local.open,
          [`drawer--${placement}`]: true,
          [`drawer--md-${mdPlacement}`]: !!mdPlacement,
          [`drawer--lg-${lgPlacement}`]: !!lgPlacement,
          ...local.classList,
        }}
        {...others}
      >
        <div class="drawer__content" {...(local.contentProps || {})}>
          {local.children}
        </div>
        <div
          class="drawer__overlay"
          onClick={local.onClose}
          {...(local.overlayProps || {})}
        ></div>
      </div>
    </Portal>
  );
};

export default Drawer;
