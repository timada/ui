import {
  Component,
  JSX,
  splitProps,
  createSignal,
  onCleanup,
  Show,
} from "solid-js";

import "./Topbar.css";

export interface TopbarProps {
  title: string;
}

const Topbar: Component<
  TopbarProps & JSX.BaseHTMLAttributes<HTMLDivElement>
> = (props) => {
  const [local, others] = splitProps(props, ["title", "children", "classList"]);
  const [onTop, setOnTop] = createSignal(false);
  let topbar: HTMLDivElement | undefined;

  const onScroll = () => {
    const height = topbar?.offsetHeight || 1;

    if (window.scrollY >= height && !onTop()) {
      setOnTop(true);
    } else if (window.scrollY < height && onTop()) {
      setOnTop(false);
    }
  };

  document.addEventListener("scroll", onScroll);

  onCleanup(() => {
    document.removeEventListener("scroll", onScroll);
  });

  return (
    <>
      <Show when={onTop()}>
        <div class="topbar-box"></div>
      </Show>
      <div
        ref={topbar}
        classList={{
          topbar: true,
          "topbar--on-top": onTop(),
          ...local.classList,
        }}
        {...others}
      >
        <div class="topbar__container">
          <div class="topbar__title">
            <h2>{local.title}</h2>
          </div>
          <div class="topbar__actions">{local.children}</div>
        </div>
      </div>
    </>
  );
};

export default Topbar;
