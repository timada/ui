import { Component } from "solid-js";

const Separator: Component = () => {
  return (
    <div>
      <h2>Separator</h2>
    </div>
  );
};

export default Separator;
