import { Component, JSX, splitProps } from "solid-js";

import "./Button.css";

export interface ButtonProps {
  variant?: "transparent";
}

const Button: Component<
  ButtonProps & JSX.ButtonHTMLAttributes<HTMLButtonElement>
> = (props) => {
  const [local, others] = splitProps(props, ["classList", "variant"]);

  return (
    <button
      classList={{
        button: true,
        [`button--${local.variant}`]: !!local.variant,
        ...local.classList,
      }}
      {...others}
    />
  );
};

export default Button;
