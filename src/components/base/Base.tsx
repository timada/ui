import { Component, onCleanup } from "solid-js";
import { createDebounce } from "utils";

import "./Base.css";

const Base: Component = (props) => {
  const removeScrollbar = createDebounce((target: HTMLElement) => {
    target.classList.remove("on-scrollbar");
  }, 500);

  const onScroll = (e: Event) => {
    let target: HTMLElement = e.target as HTMLElement;

    if (target.nodeName === "#document") {
      target = document.body;
    }

    if (!target.classList.contains("on-scrollbar")) {
      target.classList.add("on-scrollbar");
    }

    removeScrollbar(target);
  };

  window.addEventListener("scroll", onScroll, true);

  const removeAnimationStoper = createDebounce(() => {
    document.body.classList.remove("resize-animation-stopper");
  }, 400);

  const onResize = () => {
    document.body.classList.add("resize-animation-stopper");

    removeAnimationStoper();
  };

  window.addEventListener("resize", onResize);

  onCleanup(() => {
    window.removeEventListener("scroll", onScroll, true);
    window.removeEventListener("resize", onResize);
  });

  return <>{props.children}</>;
};

export default Base;
