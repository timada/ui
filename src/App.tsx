import { Component } from "solid-js";
import { useRoutes } from "solid-app-router";
import routes from "pages/routes";

const App: Component = () => {
  const Routes = useRoutes(routes);

  return <Routes />;
};

export default App;
