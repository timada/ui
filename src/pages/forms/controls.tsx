import { Topbar } from "lib";
import { Component } from "solid-js";

const controls: Component = () => {
  return (
    <>
      <Topbar title="Controls" />
    </>
  );
};

export default controls;
