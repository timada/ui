import { lazy } from "solid-js";

export const ROUTES = {
  INDEX: "/",
  SERVICES: {
    ICONS: {
      DUOTUNE: "/services/icons/duotune",
    },
  },
};

export default [
  {
    path: "/services/icons",
    component: lazy(() => import("./services/icons/Layout")),
    children: [
      {
        path: "/",
        component: lazy(() => import("./services/icons")),
      },
      {
        path: "/duotune",
        component: lazy(() => import("./services/icons/duotune")),
      },
    ],
  },
  {
    path: "/",
    component: lazy(() => import("./Layout")),
    children: [
      {
        path: "/",
        component: lazy(() => import("./index")),
      },
      {
        path: "/forms/controls",
        component: lazy(() => import("./forms/controls")),
      },
    ],
  },
];
