import { Component, createResource, Show } from "solid-js";
import { Outlet } from "solid-app-router";
import { Layout as TLayout } from "lib";
import { LayoutNav, LayoutService } from "components/layout/Layout";

const Layout: Component = () => {
  const title = "Timada UI - Base";

  document.title = title;

  const [services] = createResource<LayoutService[]>(async () =>
    (await fetch("/services.json")).json()
  );

  const navigations: LayoutNav[] = [
    {
      items: [{ href: "/", label: "Buttons" }],
    },
    {
      title: "Forms",
      items: [{ href: "/forms/controls", label: "Controls" }],
    },
  ];

  return (
    <Show when={services()}>
      <TLayout
        header={title}
        navigations={navigations}
        services={services() || []}
      >
        <Outlet />
      </TLayout>
    </Show>
  );
};

export default Layout;
