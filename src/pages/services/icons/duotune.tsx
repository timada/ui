import { Component } from "solid-js";

import {
  Topbar,
  Gra006,
  Gra007,
  Gra011,
  Gra002,
  Gra008,
  Gra004,
  Gra009,
  Gra003,
  Gra012,
  Gra010,
  Gra005,
  Gra001,
  Art003,
  Art004,
  Art005,
  Art002,
  Art009,
  Art006,
  Art001,
  Art007,
  Art008,
  Art010,
  Elc008,
  Elc002,
  Elc009,
  Elc001,
  Elc006,
  Elc005,
  Elc007,
  Elc004,
  Elc010,
  Elc003,
  Arr056,
  Arr071,
  Arr024,
  Arr057,
  Arr017,
  Arr007,
  Arr077,
  Arr036,
  Arr002,
  Arr014,
  Arr020,
  Arr092,
  Arr016,
  Arr074,
  Arr082,
  Arr088,
  Arr067,
  Arr041,
  Arr013,
  Arr010,
  Arr075,
  Arr091,
  Arr001,
  Arr040,
  Arr066,
  Arr055,
  Arr081,
  Arr028,
  Arr089,
  Arr033,
  Arr035,
  Arr005,
  Arr048,
  Arr027,
  Arr072,
  Arr080,
  Arr032,
  Arr006,
  Arr039,
  Arr054,
  Arr090,
  Arr037,
  Arr008,
  Arr018,
  Arr079,
  Arr063,
  Arr047,
  Arr023,
  Arr009,
  Arr087,
  Arr015,
  Arr050,
  Arr070,
  Arr043,
  Arr038,
  Arr073,
  Arr004,
  Arr061,
  Arr021,
  Arr045,
  Arr030,
  Arr025,
  Arr086,
  Arr042,
  Arr062,
  Arr044,
  Arr031,
  Arr059,
  Arr069,
  Arr078,
  Arr052,
  Arr003,
  Arr026,
  Arr029,
  Arr034,
  Arr051,
  Arr046,
  Arr085,
  Arr049,
  Arr068,
  Arr065,
  Arr060,
  Arr019,
  Arr058,
  Arr076,
  Arr084,
  Arr022,
  Arr053,
  Arr011,
  Arr064,
  Arr012,
  Soc010,
  Soc001,
  Soc008,
  Soc003,
  Soc002,
  Soc004,
  Soc009,
  Soc006,
  Soc005,
  Soc007,
  Abs008,
  Abs040,
  Abs029,
  Abs044,
  Abs031,
  Abs004,
  Abs048,
  Abs019,
  Abs007,
  Abs052,
  Abs015,
  Abs024,
  Abs041,
  Abs020,
  Abs042,
  Abs036,
  Abs025,
  Abs033,
  Abs006,
  Abs039,
  Abs002,
  Abs035,
  Abs005,
  Abs045,
  Abs046,
  Abs027,
  Abs003,
  Abs012,
  Abs018,
  Abs022,
  Abs028,
  Abs014,
  Abs001,
  Abs026,
  Abs051,
  Abs023,
  Abs034,
  Abs021,
  Abs016,
  Abs043,
  Abs037,
  Abs032,
  Abs049,
  Abs011,
  Abs013,
  Abs047,
  Abs050,
  Abs010,
  Abs009,
  Abs038,
  Abs017,
  Abs030,
  Fil005,
  Fil021,
  Fil020,
  Fil023,
  Fil003,
  Fil022,
  Fil016,
  Fil007,
  Fil018,
  Fil010,
  Fil012,
  Fil024,
  Fil017,
  Fil015,
  Fil006,
  Fil008,
  Fil011,
  Fil013,
  Fil001,
  Fil009,
  Fil004,
  Fil014,
  Fil002,
  Fil019,
  Fil025,
  Med005,
  Med004,
  Med009,
  Med001,
  Med003,
  Med007,
  Med008,
  Med002,
  Med006,
  Med010,
  Ecm006,
  Ecm011,
  Ecm001,
  Ecm002,
  Ecm007,
  Ecm003,
  Ecm009,
  Ecm004,
  Ecm008,
  Ecm005,
  Ecm010,
  Teh003,
  Teh002,
  Teh006,
  Teh008,
  Teh009,
  Teh007,
  Teh005,
  Teh004,
  Teh001,
  Teh010,
  Cod007,
  Cod006,
  Cod009,
  Cod008,
  Cod010,
  Cod001,
  Cod004,
  Cod005,
  Cod003,
  Cod002,
  Com005,
  Com012,
  Com009,
  Com007,
  Com004,
  Com011,
  Com010,
  Com001,
  Com003,
  Com006,
  Com002,
  Com008,
  Fin001,
  Fin010,
  Fin009,
  Fin003,
  Fin007,
  Fin002,
  Fin004,
  Fin006,
  Fin005,
  Fin008,
  Map007,
  Map003,
  Map002,
  Map008,
  Map006,
  Map005,
  Map001,
  Map010,
  Map009,
  Map004,
  Txt002,
  Txt005,
  Txt006,
  Txt001,
  Txt003,
  Txt009,
  Txt004,
  Txt010,
  Txt007,
  Txt008,
  Lay009,
  Lay007,
  Lay006,
  Lay001,
  Lay005,
  Lay003,
  Lay002,
  Lay008,
  Lay010,
  Lay004,
  Gen015,
  Gen052,
  Gen055,
  Gen021,
  Gen018,
  Gen007,
  Gen056,
  Gen033,
  Gen032,
  Gen016,
  Gen054,
  Gen044,
  Gen036,
  Gen046,
  Gen037,
  Gen009,
  Gen047,
  Gen053,
  Gen051,
  Gen043,
  Gen019,
  Gen028,
  Gen013,
  Gen001,
  Gen049,
  Gen010,
  Gen004,
  Gen038,
  Gen026,
  Gen012,
  Gen023,
  Gen034,
  Gen040,
  Gen048,
  Gen008,
  Gen035,
  Gen027,
  Gen011,
  Gen025,
  Gen005,
  Gen029,
  Gen024,
  Gen022,
  Gen039,
  Gen014,
  Gen050,
  Gen003,
  Gen031,
  Gen017,
  Gen030,
  Gen042,
  Gen006,
  Gen041,
  Gen002,
  Gen020,
  Gen045,
} from "lib";

import styles from "./duotune.module.css";

const duotune: Component = () => {
  const onCopy = (text: string) => {
    navigator.clipboard.writeText(`<${text} />`);
  };

  return (
    <>
      <Topbar title="Icons" />
      <div class={styles.icons}>
        <h2>graphs</h2>
        <div>
          <span onClick={[onCopy, "Gra006"]}>
            <Gra006 />
          </span>
          <span onClick={[onCopy, "Gra007"]}>
            <Gra007 />
          </span>
          <span onClick={[onCopy, "Gra011"]}>
            <Gra011 />
          </span>
          <span onClick={[onCopy, "Gra002"]}>
            <Gra002 />
          </span>
          <span onClick={[onCopy, "Gra008"]}>
            <Gra008 />
          </span>
          <span onClick={[onCopy, "Gra004"]}>
            <Gra004 />
          </span>
          <span onClick={[onCopy, "Gra009"]}>
            <Gra009 />
          </span>
          <span onClick={[onCopy, "Gra003"]}>
            <Gra003 />
          </span>
          <span onClick={[onCopy, "Gra012"]}>
            <Gra012 />
          </span>
          <span onClick={[onCopy, "Gra010"]}>
            <Gra010 />
          </span>
          <span onClick={[onCopy, "Gra005"]}>
            <Gra005 />
          </span>
          <span onClick={[onCopy, "Gra001"]}>
            <Gra001 />
          </span>
        </div>

        <h2>art</h2>
        <div>
          <span onClick={[onCopy, "Art003"]}>
            <Art003 />
          </span>
          <span onClick={[onCopy, "Art004"]}>
            <Art004 />
          </span>
          <span onClick={[onCopy, "Art005"]}>
            <Art005 />
          </span>
          <span onClick={[onCopy, "Art002"]}>
            <Art002 />
          </span>
          <span onClick={[onCopy, "Art009"]}>
            <Art009 />
          </span>
          <span onClick={[onCopy, "Art006"]}>
            <Art006 />
          </span>
          <span onClick={[onCopy, "Art001"]}>
            <Art001 />
          </span>
          <span onClick={[onCopy, "Art007"]}>
            <Art007 />
          </span>
          <span onClick={[onCopy, "Art008"]}>
            <Art008 />
          </span>
          <span onClick={[onCopy, "Art010"]}>
            <Art010 />
          </span>
        </div>

        <h2>electronics</h2>
        <div>
          <span onClick={[onCopy, "Elc008"]}>
            <Elc008 />
          </span>
          <span onClick={[onCopy, "Elc002"]}>
            <Elc002 />
          </span>
          <span onClick={[onCopy, "Elc009"]}>
            <Elc009 />
          </span>
          <span onClick={[onCopy, "Elc001"]}>
            <Elc001 />
          </span>
          <span onClick={[onCopy, "Elc006"]}>
            <Elc006 />
          </span>
          <span onClick={[onCopy, "Elc005"]}>
            <Elc005 />
          </span>
          <span onClick={[onCopy, "Elc007"]}>
            <Elc007 />
          </span>
          <span onClick={[onCopy, "Elc004"]}>
            <Elc004 />
          </span>
          <span onClick={[onCopy, "Elc010"]}>
            <Elc010 />
          </span>
          <span onClick={[onCopy, "Elc003"]}>
            <Elc003 />
          </span>
        </div>

        <h2>arrows</h2>
        <div>
          <span onClick={[onCopy, "Arr056"]}>
            <Arr056 />
          </span>
          <span onClick={[onCopy, "Arr071"]}>
            <Arr071 />
          </span>
          <span onClick={[onCopy, "Arr024"]}>
            <Arr024 />
          </span>
          <span onClick={[onCopy, "Arr057"]}>
            <Arr057 />
          </span>
          <span onClick={[onCopy, "Arr017"]}>
            <Arr017 />
          </span>
          <span onClick={[onCopy, "Arr007"]}>
            <Arr007 />
          </span>
          <span onClick={[onCopy, "Arr077"]}>
            <Arr077 />
          </span>
          <span onClick={[onCopy, "Arr036"]}>
            <Arr036 />
          </span>
          <span onClick={[onCopy, "Arr002"]}>
            <Arr002 />
          </span>
          <span onClick={[onCopy, "Arr014"]}>
            <Arr014 />
          </span>
          <span onClick={[onCopy, "Arr020"]}>
            <Arr020 />
          </span>
          <span onClick={[onCopy, "Arr092"]}>
            <Arr092 />
          </span>
          <span onClick={[onCopy, "Arr016"]}>
            <Arr016 />
          </span>
          <span onClick={[onCopy, "Arr074"]}>
            <Arr074 />
          </span>
          <span onClick={[onCopy, "Arr082"]}>
            <Arr082 />
          </span>
          <span onClick={[onCopy, "Arr088"]}>
            <Arr088 />
          </span>
          <span onClick={[onCopy, "Arr067"]}>
            <Arr067 />
          </span>
          <span onClick={[onCopy, "Arr041"]}>
            <Arr041 />
          </span>
          <span onClick={[onCopy, "Arr013"]}>
            <Arr013 />
          </span>
          <span onClick={[onCopy, "Arr010"]}>
            <Arr010 />
          </span>
          <span onClick={[onCopy, "Arr075"]}>
            <Arr075 />
          </span>
          <span onClick={[onCopy, "Arr091"]}>
            <Arr091 />
          </span>
          <span onClick={[onCopy, "Arr001"]}>
            <Arr001 />
          </span>
          <span onClick={[onCopy, "Arr040"]}>
            <Arr040 />
          </span>
          <span onClick={[onCopy, "Arr066"]}>
            <Arr066 />
          </span>
          <span onClick={[onCopy, "Arr055"]}>
            <Arr055 />
          </span>
          <span onClick={[onCopy, "Arr081"]}>
            <Arr081 />
          </span>
          <span onClick={[onCopy, "Arr028"]}>
            <Arr028 />
          </span>
          <span onClick={[onCopy, "Arr089"]}>
            <Arr089 />
          </span>
          <span onClick={[onCopy, "Arr033"]}>
            <Arr033 />
          </span>
          <span onClick={[onCopy, "Arr035"]}>
            <Arr035 />
          </span>
          <span onClick={[onCopy, "Arr005"]}>
            <Arr005 />
          </span>
          <span onClick={[onCopy, "Arr048"]}>
            <Arr048 />
          </span>
          <span onClick={[onCopy, "Arr027"]}>
            <Arr027 />
          </span>
          <span onClick={[onCopy, "Arr072"]}>
            <Arr072 />
          </span>
          <span onClick={[onCopy, "Arr080"]}>
            <Arr080 />
          </span>
          <span onClick={[onCopy, "Arr032"]}>
            <Arr032 />
          </span>
          <span onClick={[onCopy, "Arr006"]}>
            <Arr006 />
          </span>
          <span onClick={[onCopy, "Arr039"]}>
            <Arr039 />
          </span>
          <span onClick={[onCopy, "Arr054"]}>
            <Arr054 />
          </span>
          <span onClick={[onCopy, "Arr090"]}>
            <Arr090 />
          </span>
          <span onClick={[onCopy, "Arr037"]}>
            <Arr037 />
          </span>
          <span onClick={[onCopy, "Arr008"]}>
            <Arr008 />
          </span>
          <span onClick={[onCopy, "Arr018"]}>
            <Arr018 />
          </span>
          <span onClick={[onCopy, "Arr079"]}>
            <Arr079 />
          </span>
          <span onClick={[onCopy, "Arr063"]}>
            <Arr063 />
          </span>
          <span onClick={[onCopy, "Arr047"]}>
            <Arr047 />
          </span>
          <span onClick={[onCopy, "Arr023"]}>
            <Arr023 />
          </span>
          <span onClick={[onCopy, "Arr009"]}>
            <Arr009 />
          </span>
          <span onClick={[onCopy, "Arr087"]}>
            <Arr087 />
          </span>
          <span onClick={[onCopy, "Arr015"]}>
            <Arr015 />
          </span>
          <span onClick={[onCopy, "Arr050"]}>
            <Arr050 />
          </span>
          <span onClick={[onCopy, "Arr070"]}>
            <Arr070 />
          </span>
          <span onClick={[onCopy, "Arr043"]}>
            <Arr043 />
          </span>
          <span onClick={[onCopy, "Arr038"]}>
            <Arr038 />
          </span>
          <span onClick={[onCopy, "Arr073"]}>
            <Arr073 />
          </span>
          <span onClick={[onCopy, "Arr004"]}>
            <Arr004 />
          </span>
          <span onClick={[onCopy, "Arr061"]}>
            <Arr061 />
          </span>
          <span onClick={[onCopy, "Arr021"]}>
            <Arr021 />
          </span>
          <span onClick={[onCopy, "Arr045"]}>
            <Arr045 />
          </span>
          <span onClick={[onCopy, "Arr030"]}>
            <Arr030 />
          </span>
          <span onClick={[onCopy, "Arr025"]}>
            <Arr025 />
          </span>
          <span onClick={[onCopy, "Arr086"]}>
            <Arr086 />
          </span>
          <span onClick={[onCopy, "Arr042"]}>
            <Arr042 />
          </span>
          <span onClick={[onCopy, "Arr062"]}>
            <Arr062 />
          </span>
          <span onClick={[onCopy, "Arr044"]}>
            <Arr044 />
          </span>
          <span onClick={[onCopy, "Arr031"]}>
            <Arr031 />
          </span>
          <span onClick={[onCopy, "Arr059"]}>
            <Arr059 />
          </span>
          <span onClick={[onCopy, "Arr069"]}>
            <Arr069 />
          </span>
          <span onClick={[onCopy, "Arr078"]}>
            <Arr078 />
          </span>
          <span onClick={[onCopy, "Arr052"]}>
            <Arr052 />
          </span>
          <span onClick={[onCopy, "Arr003"]}>
            <Arr003 />
          </span>
          <span onClick={[onCopy, "Arr026"]}>
            <Arr026 />
          </span>
          <span onClick={[onCopy, "Arr029"]}>
            <Arr029 />
          </span>
          <span onClick={[onCopy, "Arr034"]}>
            <Arr034 />
          </span>
          <span onClick={[onCopy, "Arr051"]}>
            <Arr051 />
          </span>
          <span onClick={[onCopy, "Arr046"]}>
            <Arr046 />
          </span>
          <span onClick={[onCopy, "Arr085"]}>
            <Arr085 />
          </span>
          <span onClick={[onCopy, "Arr049"]}>
            <Arr049 />
          </span>
          <span onClick={[onCopy, "Arr068"]}>
            <Arr068 />
          </span>
          <span onClick={[onCopy, "Arr065"]}>
            <Arr065 />
          </span>
          <span onClick={[onCopy, "Arr060"]}>
            <Arr060 />
          </span>
          <span onClick={[onCopy, "Arr019"]}>
            <Arr019 />
          </span>
          <span onClick={[onCopy, "Arr058"]}>
            <Arr058 />
          </span>
          <span onClick={[onCopy, "Arr076"]}>
            <Arr076 />
          </span>
          <span onClick={[onCopy, "Arr084"]}>
            <Arr084 />
          </span>
          <span onClick={[onCopy, "Arr022"]}>
            <Arr022 />
          </span>
          <span onClick={[onCopy, "Arr053"]}>
            <Arr053 />
          </span>
          <span onClick={[onCopy, "Arr011"]}>
            <Arr011 />
          </span>
          <span onClick={[onCopy, "Arr064"]}>
            <Arr064 />
          </span>
          <span onClick={[onCopy, "Arr012"]}>
            <Arr012 />
          </span>
        </div>

        <h2>social</h2>
        <div>
          <span onClick={[onCopy, "Soc010"]}>
            <Soc010 />
          </span>
          <span onClick={[onCopy, "Soc001"]}>
            <Soc001 />
          </span>
          <span onClick={[onCopy, "Soc008"]}>
            <Soc008 />
          </span>
          <span onClick={[onCopy, "Soc003"]}>
            <Soc003 />
          </span>
          <span onClick={[onCopy, "Soc002"]}>
            <Soc002 />
          </span>
          <span onClick={[onCopy, "Soc004"]}>
            <Soc004 />
          </span>
          <span onClick={[onCopy, "Soc009"]}>
            <Soc009 />
          </span>
          <span onClick={[onCopy, "Soc006"]}>
            <Soc006 />
          </span>
          <span onClick={[onCopy, "Soc005"]}>
            <Soc005 />
          </span>
          <span onClick={[onCopy, "Soc007"]}>
            <Soc007 />
          </span>
        </div>

        <h2>abstract</h2>
        <div>
          <span onClick={[onCopy, "Abs008"]}>
            <Abs008 />
          </span>
          <span onClick={[onCopy, "Abs040"]}>
            <Abs040 />
          </span>
          <span onClick={[onCopy, "Abs029"]}>
            <Abs029 />
          </span>
          <span onClick={[onCopy, "Abs044"]}>
            <Abs044 />
          </span>
          <span onClick={[onCopy, "Abs031"]}>
            <Abs031 />
          </span>
          <span onClick={[onCopy, "Abs004"]}>
            <Abs004 />
          </span>
          <span onClick={[onCopy, "Abs048"]}>
            <Abs048 />
          </span>
          <span onClick={[onCopy, "Abs019"]}>
            <Abs019 />
          </span>
          <span onClick={[onCopy, "Abs007"]}>
            <Abs007 />
          </span>
          <span onClick={[onCopy, "Abs052"]}>
            <Abs052 />
          </span>
          <span onClick={[onCopy, "Abs015"]}>
            <Abs015 />
          </span>
          <span onClick={[onCopy, "Abs024"]}>
            <Abs024 />
          </span>
          <span onClick={[onCopy, "Abs041"]}>
            <Abs041 />
          </span>
          <span onClick={[onCopy, "Abs020"]}>
            <Abs020 />
          </span>
          <span onClick={[onCopy, "Abs042"]}>
            <Abs042 />
          </span>
          <span onClick={[onCopy, "Abs036"]}>
            <Abs036 />
          </span>
          <span onClick={[onCopy, "Abs025"]}>
            <Abs025 />
          </span>
          <span onClick={[onCopy, "Abs033"]}>
            <Abs033 />
          </span>
          <span onClick={[onCopy, "Abs006"]}>
            <Abs006 />
          </span>
          <span onClick={[onCopy, "Abs039"]}>
            <Abs039 />
          </span>
          <span onClick={[onCopy, "Abs002"]}>
            <Abs002 />
          </span>
          <span onClick={[onCopy, "Abs035"]}>
            <Abs035 />
          </span>
          <span onClick={[onCopy, "Abs005"]}>
            <Abs005 />
          </span>
          <span onClick={[onCopy, "Abs045"]}>
            <Abs045 />
          </span>
          <span onClick={[onCopy, "Abs046"]}>
            <Abs046 />
          </span>
          <span onClick={[onCopy, "Abs027"]}>
            <Abs027 />
          </span>
          <span onClick={[onCopy, "Abs003"]}>
            <Abs003 />
          </span>
          <span onClick={[onCopy, "Abs012"]}>
            <Abs012 />
          </span>
          <span onClick={[onCopy, "Abs018"]}>
            <Abs018 />
          </span>
          <span onClick={[onCopy, "Abs022"]}>
            <Abs022 />
          </span>
          <span onClick={[onCopy, "Abs028"]}>
            <Abs028 />
          </span>
          <span onClick={[onCopy, "Abs014"]}>
            <Abs014 />
          </span>
          <span onClick={[onCopy, "Abs001"]}>
            <Abs001 />
          </span>
          <span onClick={[onCopy, "Abs026"]}>
            <Abs026 />
          </span>
          <span onClick={[onCopy, "Abs051"]}>
            <Abs051 />
          </span>
          <span onClick={[onCopy, "Abs023"]}>
            <Abs023 />
          </span>
          <span onClick={[onCopy, "Abs034"]}>
            <Abs034 />
          </span>
          <span onClick={[onCopy, "Abs021"]}>
            <Abs021 />
          </span>
          <span onClick={[onCopy, "Abs016"]}>
            <Abs016 />
          </span>
          <span onClick={[onCopy, "Abs043"]}>
            <Abs043 />
          </span>
          <span onClick={[onCopy, "Abs037"]}>
            <Abs037 />
          </span>
          <span onClick={[onCopy, "Abs032"]}>
            <Abs032 />
          </span>
          <span onClick={[onCopy, "Abs049"]}>
            <Abs049 />
          </span>
          <span onClick={[onCopy, "Abs011"]}>
            <Abs011 />
          </span>
          <span onClick={[onCopy, "Abs013"]}>
            <Abs013 />
          </span>
          <span onClick={[onCopy, "Abs047"]}>
            <Abs047 />
          </span>
          <span onClick={[onCopy, "Abs050"]}>
            <Abs050 />
          </span>
          <span onClick={[onCopy, "Abs010"]}>
            <Abs010 />
          </span>
          <span onClick={[onCopy, "Abs009"]}>
            <Abs009 />
          </span>
          <span onClick={[onCopy, "Abs038"]}>
            <Abs038 />
          </span>
          <span onClick={[onCopy, "Abs017"]}>
            <Abs017 />
          </span>
          <span onClick={[onCopy, "Abs030"]}>
            <Abs030 />
          </span>
        </div>

        <h2>files</h2>
        <div>
          <span onClick={[onCopy, "Fil005"]}>
            <Fil005 />
          </span>
          <span onClick={[onCopy, "Fil021"]}>
            <Fil021 />
          </span>
          <span onClick={[onCopy, "Fil020"]}>
            <Fil020 />
          </span>
          <span onClick={[onCopy, "Fil023"]}>
            <Fil023 />
          </span>
          <span onClick={[onCopy, "Fil003"]}>
            <Fil003 />
          </span>
          <span onClick={[onCopy, "Fil022"]}>
            <Fil022 />
          </span>
          <span onClick={[onCopy, "Fil016"]}>
            <Fil016 />
          </span>
          <span onClick={[onCopy, "Fil007"]}>
            <Fil007 />
          </span>
          <span onClick={[onCopy, "Fil018"]}>
            <Fil018 />
          </span>
          <span onClick={[onCopy, "Fil010"]}>
            <Fil010 />
          </span>
          <span onClick={[onCopy, "Fil012"]}>
            <Fil012 />
          </span>
          <span onClick={[onCopy, "Fil024"]}>
            <Fil024 />
          </span>
          <span onClick={[onCopy, "Fil017"]}>
            <Fil017 />
          </span>
          <span onClick={[onCopy, "Fil015"]}>
            <Fil015 />
          </span>
          <span onClick={[onCopy, "Fil006"]}>
            <Fil006 />
          </span>
          <span onClick={[onCopy, "Fil008"]}>
            <Fil008 />
          </span>
          <span onClick={[onCopy, "Fil011"]}>
            <Fil011 />
          </span>
          <span onClick={[onCopy, "Fil013"]}>
            <Fil013 />
          </span>
          <span onClick={[onCopy, "Fil001"]}>
            <Fil001 />
          </span>
          <span onClick={[onCopy, "Fil009"]}>
            <Fil009 />
          </span>
          <span onClick={[onCopy, "Fil004"]}>
            <Fil004 />
          </span>
          <span onClick={[onCopy, "Fil014"]}>
            <Fil014 />
          </span>
          <span onClick={[onCopy, "Fil002"]}>
            <Fil002 />
          </span>
          <span onClick={[onCopy, "Fil019"]}>
            <Fil019 />
          </span>
          <span onClick={[onCopy, "Fil025"]}>
            <Fil025 />
          </span>
        </div>

        <h2>medicine</h2>
        <div>
          <span onClick={[onCopy, "Med005"]}>
            <Med005 />
          </span>
          <span onClick={[onCopy, "Med004"]}>
            <Med004 />
          </span>
          <span onClick={[onCopy, "Med009"]}>
            <Med009 />
          </span>
          <span onClick={[onCopy, "Med001"]}>
            <Med001 />
          </span>
          <span onClick={[onCopy, "Med003"]}>
            <Med003 />
          </span>
          <span onClick={[onCopy, "Med007"]}>
            <Med007 />
          </span>
          <span onClick={[onCopy, "Med008"]}>
            <Med008 />
          </span>
          <span onClick={[onCopy, "Med002"]}>
            <Med002 />
          </span>
          <span onClick={[onCopy, "Med006"]}>
            <Med006 />
          </span>
          <span onClick={[onCopy, "Med010"]}>
            <Med010 />
          </span>
        </div>

        <h2>ecommerce</h2>
        <div>
          <span onClick={[onCopy, "Ecm006"]}>
            <Ecm006 />
          </span>
          <span onClick={[onCopy, "Ecm011"]}>
            <Ecm011 />
          </span>
          <span onClick={[onCopy, "Ecm001"]}>
            <Ecm001 />
          </span>
          <span onClick={[onCopy, "Ecm002"]}>
            <Ecm002 />
          </span>
          <span onClick={[onCopy, "Ecm007"]}>
            <Ecm007 />
          </span>
          <span onClick={[onCopy, "Ecm003"]}>
            <Ecm003 />
          </span>
          <span onClick={[onCopy, "Ecm009"]}>
            <Ecm009 />
          </span>
          <span onClick={[onCopy, "Ecm004"]}>
            <Ecm004 />
          </span>
          <span onClick={[onCopy, "Ecm008"]}>
            <Ecm008 />
          </span>
          <span onClick={[onCopy, "Ecm005"]}>
            <Ecm005 />
          </span>
          <span onClick={[onCopy, "Ecm010"]}>
            <Ecm010 />
          </span>
        </div>

        <h2>technology</h2>
        <div>
          <span onClick={[onCopy, "Teh003"]}>
            <Teh003 />
          </span>
          <span onClick={[onCopy, "Teh002"]}>
            <Teh002 />
          </span>
          <span onClick={[onCopy, "Teh006"]}>
            <Teh006 />
          </span>
          <span onClick={[onCopy, "Teh008"]}>
            <Teh008 />
          </span>
          <span onClick={[onCopy, "Teh009"]}>
            <Teh009 />
          </span>
          <span onClick={[onCopy, "Teh007"]}>
            <Teh007 />
          </span>
          <span onClick={[onCopy, "Teh005"]}>
            <Teh005 />
          </span>
          <span onClick={[onCopy, "Teh004"]}>
            <Teh004 />
          </span>
          <span onClick={[onCopy, "Teh001"]}>
            <Teh001 />
          </span>
          <span onClick={[onCopy, "Teh010"]}>
            <Teh010 />
          </span>
        </div>

        <h2>coding</h2>
        <div>
          <span onClick={[onCopy, "Cod007"]}>
            <Cod007 />
          </span>
          <span onClick={[onCopy, "Cod006"]}>
            <Cod006 />
          </span>
          <span onClick={[onCopy, "Cod009"]}>
            <Cod009 />
          </span>
          <span onClick={[onCopy, "Cod008"]}>
            <Cod008 />
          </span>
          <span onClick={[onCopy, "Cod010"]}>
            <Cod010 />
          </span>
          <span onClick={[onCopy, "Cod001"]}>
            <Cod001 />
          </span>
          <span onClick={[onCopy, "Cod004"]}>
            <Cod004 />
          </span>
          <span onClick={[onCopy, "Cod005"]}>
            <Cod005 />
          </span>
          <span onClick={[onCopy, "Cod003"]}>
            <Cod003 />
          </span>
          <span onClick={[onCopy, "Cod002"]}>
            <Cod002 />
          </span>
        </div>

        <h2>communication</h2>
        <div>
          <span onClick={[onCopy, "Com005"]}>
            <Com005 />
          </span>
          <span onClick={[onCopy, "Com012"]}>
            <Com012 />
          </span>
          <span onClick={[onCopy, "Com009"]}>
            <Com009 />
          </span>
          <span onClick={[onCopy, "Com007"]}>
            <Com007 />
          </span>
          <span onClick={[onCopy, "Com004"]}>
            <Com004 />
          </span>
          <span onClick={[onCopy, "Com011"]}>
            <Com011 />
          </span>
          <span onClick={[onCopy, "Com010"]}>
            <Com010 />
          </span>
          <span onClick={[onCopy, "Com001"]}>
            <Com001 />
          </span>
          <span onClick={[onCopy, "Com003"]}>
            <Com003 />
          </span>
          <span onClick={[onCopy, "Com006"]}>
            <Com006 />
          </span>
          <span onClick={[onCopy, "Com002"]}>
            <Com002 />
          </span>
          <span onClick={[onCopy, "Com008"]}>
            <Com008 />
          </span>
        </div>

        <h2>finance</h2>
        <div>
          <span onClick={[onCopy, "Fin001"]}>
            <Fin001 />
          </span>
          <span onClick={[onCopy, "Fin010"]}>
            <Fin010 />
          </span>
          <span onClick={[onCopy, "Fin009"]}>
            <Fin009 />
          </span>
          <span onClick={[onCopy, "Fin003"]}>
            <Fin003 />
          </span>
          <span onClick={[onCopy, "Fin007"]}>
            <Fin007 />
          </span>
          <span onClick={[onCopy, "Fin002"]}>
            <Fin002 />
          </span>
          <span onClick={[onCopy, "Fin004"]}>
            <Fin004 />
          </span>
          <span onClick={[onCopy, "Fin006"]}>
            <Fin006 />
          </span>
          <span onClick={[onCopy, "Fin005"]}>
            <Fin005 />
          </span>
          <span onClick={[onCopy, "Fin008"]}>
            <Fin008 />
          </span>
        </div>

        <h2>maps</h2>
        <div>
          <span onClick={[onCopy, "Map007"]}>
            <Map007 />
          </span>
          <span onClick={[onCopy, "Map003"]}>
            <Map003 />
          </span>
          <span onClick={[onCopy, "Map002"]}>
            <Map002 />
          </span>
          <span onClick={[onCopy, "Map008"]}>
            <Map008 />
          </span>
          <span onClick={[onCopy, "Map006"]}>
            <Map006 />
          </span>
          <span onClick={[onCopy, "Map005"]}>
            <Map005 />
          </span>
          <span onClick={[onCopy, "Map001"]}>
            <Map001 />
          </span>
          <span onClick={[onCopy, "Map010"]}>
            <Map010 />
          </span>
          <span onClick={[onCopy, "Map009"]}>
            <Map009 />
          </span>
          <span onClick={[onCopy, "Map004"]}>
            <Map004 />
          </span>
        </div>

        <h2>text</h2>
        <div>
          <span onClick={[onCopy, "Txt002"]}>
            <Txt002 />
          </span>
          <span onClick={[onCopy, "Txt005"]}>
            <Txt005 />
          </span>
          <span onClick={[onCopy, "Txt006"]}>
            <Txt006 />
          </span>
          <span onClick={[onCopy, "Txt001"]}>
            <Txt001 />
          </span>
          <span onClick={[onCopy, "Txt003"]}>
            <Txt003 />
          </span>
          <span onClick={[onCopy, "Txt009"]}>
            <Txt009 />
          </span>
          <span onClick={[onCopy, "Txt004"]}>
            <Txt004 />
          </span>
          <span onClick={[onCopy, "Txt010"]}>
            <Txt010 />
          </span>
          <span onClick={[onCopy, "Txt007"]}>
            <Txt007 />
          </span>
          <span onClick={[onCopy, "Txt008"]}>
            <Txt008 />
          </span>
        </div>

        <h2>layouts</h2>
        <div>
          <span onClick={[onCopy, "Lay009"]}>
            <Lay009 />
          </span>
          <span onClick={[onCopy, "Lay007"]}>
            <Lay007 />
          </span>
          <span onClick={[onCopy, "Lay006"]}>
            <Lay006 />
          </span>
          <span onClick={[onCopy, "Lay001"]}>
            <Lay001 />
          </span>
          <span onClick={[onCopy, "Lay005"]}>
            <Lay005 />
          </span>
          <span onClick={[onCopy, "Lay003"]}>
            <Lay003 />
          </span>
          <span onClick={[onCopy, "Lay002"]}>
            <Lay002 />
          </span>
          <span onClick={[onCopy, "Lay008"]}>
            <Lay008 />
          </span>
          <span onClick={[onCopy, "Lay010"]}>
            <Lay010 />
          </span>
          <span onClick={[onCopy, "Lay004"]}>
            <Lay004 />
          </span>
        </div>

        <h2>general</h2>
        <div>
          <span onClick={[onCopy, "Gen015"]}>
            <Gen015 />
          </span>
          <span onClick={[onCopy, "Gen052"]}>
            <Gen052 />
          </span>
          <span onClick={[onCopy, "Gen055"]}>
            <Gen055 />
          </span>
          <span onClick={[onCopy, "Gen021"]}>
            <Gen021 />
          </span>
          <span onClick={[onCopy, "Gen018"]}>
            <Gen018 />
          </span>
          <span onClick={[onCopy, "Gen007"]}>
            <Gen007 />
          </span>
          <span onClick={[onCopy, "Gen056"]}>
            <Gen056 />
          </span>
          <span onClick={[onCopy, "Gen033"]}>
            <Gen033 />
          </span>
          <span onClick={[onCopy, "Gen032"]}>
            <Gen032 />
          </span>
          <span onClick={[onCopy, "Gen016"]}>
            <Gen016 />
          </span>
          <span onClick={[onCopy, "Gen054"]}>
            <Gen054 />
          </span>
          <span onClick={[onCopy, "Gen044"]}>
            <Gen044 />
          </span>
          <span onClick={[onCopy, "Gen036"]}>
            <Gen036 />
          </span>
          <span onClick={[onCopy, "Gen046"]}>
            <Gen046 />
          </span>
          <span onClick={[onCopy, "Gen037"]}>
            <Gen037 />
          </span>
          <span onClick={[onCopy, "Gen009"]}>
            <Gen009 />
          </span>
          <span onClick={[onCopy, "Gen047"]}>
            <Gen047 />
          </span>
          <span onClick={[onCopy, "Gen053"]}>
            <Gen053 />
          </span>
          <span onClick={[onCopy, "Gen051"]}>
            <Gen051 />
          </span>
          <span onClick={[onCopy, "Gen043"]}>
            <Gen043 />
          </span>
          <span onClick={[onCopy, "Gen019"]}>
            <Gen019 />
          </span>
          <span onClick={[onCopy, "Gen028"]}>
            <Gen028 />
          </span>
          <span onClick={[onCopy, "Gen013"]}>
            <Gen013 />
          </span>
          <span onClick={[onCopy, "Gen001"]}>
            <Gen001 />
          </span>
          <span onClick={[onCopy, "Gen049"]}>
            <Gen049 />
          </span>
          <span onClick={[onCopy, "Gen010"]}>
            <Gen010 />
          </span>
          <span onClick={[onCopy, "Gen004"]}>
            <Gen004 />
          </span>
          <span onClick={[onCopy, "Gen038"]}>
            <Gen038 />
          </span>
          <span onClick={[onCopy, "Gen026"]}>
            <Gen026 />
          </span>
          <span onClick={[onCopy, "Gen012"]}>
            <Gen012 />
          </span>
          <span onClick={[onCopy, "Gen023"]}>
            <Gen023 />
          </span>
          <span onClick={[onCopy, "Gen034"]}>
            <Gen034 />
          </span>
          <span onClick={[onCopy, "Gen040"]}>
            <Gen040 />
          </span>
          <span onClick={[onCopy, "Gen048"]}>
            <Gen048 />
          </span>
          <span onClick={[onCopy, "Gen008"]}>
            <Gen008 />
          </span>
          <span onClick={[onCopy, "Gen035"]}>
            <Gen035 />
          </span>
          <span onClick={[onCopy, "Gen027"]}>
            <Gen027 />
          </span>
          <span onClick={[onCopy, "Gen011"]}>
            <Gen011 />
          </span>
          <span onClick={[onCopy, "Gen025"]}>
            <Gen025 />
          </span>
          <span onClick={[onCopy, "Gen005"]}>
            <Gen005 />
          </span>
          <span onClick={[onCopy, "Gen029"]}>
            <Gen029 />
          </span>
          <span onClick={[onCopy, "Gen024"]}>
            <Gen024 />
          </span>
          <span onClick={[onCopy, "Gen022"]}>
            <Gen022 />
          </span>
          <span onClick={[onCopy, "Gen039"]}>
            <Gen039 />
          </span>
          <span onClick={[onCopy, "Gen014"]}>
            <Gen014 />
          </span>
          <span onClick={[onCopy, "Gen050"]}>
            <Gen050 />
          </span>
          <span onClick={[onCopy, "Gen003"]}>
            <Gen003 />
          </span>
          <span onClick={[onCopy, "Gen031"]}>
            <Gen031 />
          </span>
          <span onClick={[onCopy, "Gen017"]}>
            <Gen017 />
          </span>
          <span onClick={[onCopy, "Gen030"]}>
            <Gen030 />
          </span>
          <span onClick={[onCopy, "Gen042"]}>
            <Gen042 />
          </span>
          <span onClick={[onCopy, "Gen006"]}>
            <Gen006 />
          </span>
          <span onClick={[onCopy, "Gen041"]}>
            <Gen041 />
          </span>
          <span onClick={[onCopy, "Gen002"]}>
            <Gen002 />
          </span>
          <span onClick={[onCopy, "Gen020"]}>
            <Gen020 />
          </span>
          <span onClick={[onCopy, "Gen045"]}>
            <Gen045 />
          </span>
        </div>
      </div>
    </>
  );
};

export default duotune;
