import { ROUTES } from "pages/routes";
import { Navigate } from "solid-app-router";
import { Component } from "solid-js";

const index: Component = () => {
  return <Navigate href={ROUTES.SERVICES.ICONS.DUOTUNE} />;
};

export default index;
