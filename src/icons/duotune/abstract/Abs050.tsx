import { Component, JSX } from "solid-js";

const Abs050: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24px"
      height="24px"
      viewBox="0 0 24 24"
      version="1.1"
      {...props}
    >
      <circle fill="currentColor" cx="12" cy="12" r="8" />
    </svg>
  );
};

export default Abs050;
