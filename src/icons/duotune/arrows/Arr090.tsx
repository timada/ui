import { Component, JSX } from "solid-js";

const Arr090: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect
        x="4.36396"
        y="11.364"
        width="16"
        height="2"
        rx="1"
        fill="currentColor"
      />
    </svg>
  );
};

export default Arr090;
