import { Component, JSX } from "solid-js";

const Arr026: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M7.75 10.7L17.65 5V19L7.75 13.3C6.75 12.7 6.75 11.3 7.75 10.7Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default Arr026;
