import { Component, JSX } from "solid-js";

const Arr089: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect x="6" y="11" width="12" height="2" rx="1" fill="currentColor" />
    </svg>
  );
};

export default Arr089;
