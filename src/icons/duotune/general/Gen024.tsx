import { Component, JSX } from "solid-js";

const Gen024: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24px"
      height="24px"
      viewBox="0 0 24 24"
      {...props}
    >
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="5" y="5" width="5" height="5" rx="1" fill="currentColor" />
        <rect
          x="14"
          y="5"
          width="5"
          height="5"
          rx="1"
          fill="currentColor"
          opacity="0.3"
        />
        <rect
          x="5"
          y="14"
          width="5"
          height="5"
          rx="1"
          fill="currentColor"
          opacity="0.3"
        />
        <rect
          x="14"
          y="14"
          width="5"
          height="5"
          rx="1"
          fill="currentColor"
          opacity="0.3"
        />
      </g>
    </svg>
  );
};

export default Gen024;
