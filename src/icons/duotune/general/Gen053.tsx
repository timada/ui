import { Component, JSX } from "solid-js";

const Gen053: Component<JSX.SvgSVGAttributes<SVGSVGElement>> = (props) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect x="10" y="10" width="4" height="4" rx="2" fill="currentColor" />
      <rect x="10" y="3" width="4" height="4" rx="2" fill="currentColor" />
      <rect x="10" y="17" width="4" height="4" rx="2" fill="currentColor" />
    </svg>
  );
};

export default Gen053;
