import { enableMapSet } from "immer";
import { TimadaUI } from "lib";
import { Router } from "solid-app-router";
import { render } from "solid-js/web";

import App from "./App";

enableMapSet();

render(
  () => (
    <TimadaUI>
      <Router>
        <App />
      </Router>
    </TimadaUI>
  ),
  document.getElementById("root") as HTMLElement
);
